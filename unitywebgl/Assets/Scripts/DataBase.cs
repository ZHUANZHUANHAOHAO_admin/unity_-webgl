using System;

using UnityEngine;

[Serializable]
public class DataBase
{
    public override string ToString()
    {
        return JsonUtility.ToJson(this, true);
    }
}

/// <summary>
/// 数据壳
/// </summary>
[Serializable]
public class DataShells<T> : DataBase where T : DataBase
{
    public int code;
    public string msg;
    public T[] data;
}

/// <summary>
/// 数据壳
/// </summary>
/// <typeparam name="T"></typeparam>
[Serializable]
public class DataShell<T> : DataBase where T : DataBase
{
    public T data;
    public int errorcode;
    public string msg;
    public int ret;
}
/// <summary>
/// 数据壳
/// </summary>
/// <typeparam name="T"></typeparam>
[Serializable]
public class DatasShell<T> : DataBase where T : DataBase
{
    public T[] data;
    public int errorcode;
    public string msg;
    public int ret;
}
/// <summary>
/// 配置文件数据类型
/// </summary>
[Serializable]
public class ConfigURLData : DataBase
{
    /// <summary>
    /// url
    /// </summary>
    public string url1;
}