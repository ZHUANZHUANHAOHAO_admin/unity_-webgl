using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Proyecto26;
using RenderHeads.Media.AVProVideo;
using UnityEngine;

public class MainManager : MonoBehaviour
{
    public MediaPlayer player;

    void Awake()
    {
        string path = Path.Combine(Application.streamingAssetsPath, "configURL.json");

        RestClient.Request(new RequestHelper
        {
            Method = "GET",
            Uri = path,
        }).Then(mresponse =>
        {
           

            DataShell<ConfigURLData> configURL = JsonUtility.FromJson<DataShell<ConfigURLData>>(mresponse.Text);
         
            player.OpenMedia(MediaPathType.AbsolutePathOrURL, configURL.data.url1);

        }).Catch(err => { Debug.LogError("--------------" + err.Message); });
    }


  
}
